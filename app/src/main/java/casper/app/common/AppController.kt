package casper.app.common

import android.content.Context
import android.content.Intent
import casper.app.PersonActivity
import casper.core.Person
import casper.core.RandomPersonProvider

object AppController {
    val settings = AppSettings()
    val persons = RandomPersonProvider()

    fun showPersonDetail(context:Context, person: Person) {
        val intent = Intent(context, PersonActivity::class.java).apply {
            putExtra(PersonActivity.PERSON_ID, person.id)
        }
        context.startActivity(intent)
    }
}