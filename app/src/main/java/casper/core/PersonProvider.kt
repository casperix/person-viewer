package casper.core

interface PersonProvider {
    fun getPersons(): List<Person>
}

