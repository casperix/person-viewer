package casper.core

import casper.util.FileUtil
import java.util.*
import kotlin.random.Random

class RandomPersonProvider : PersonProvider {
    private val names = FileUtil.readLines("res/raw/persons.txt").toSet()
    private val random = Random(0)
    private val persons = (1..100).map { Person(it.toLong(), names.random(random), randomRegisterTime(random), randomBirthday(random)) }

    override fun getPersons(): List<Person> {
        return persons
    }

    companion object {
        private val first = GregorianCalendar(1950, 1, 1).timeInMillis
        private val middle = GregorianCalendar(2000, 12, 31).timeInMillis
        private val last = GregorianCalendar(2020, 12, 31).timeInMillis

        private fun randomBirthday(random: Random): Date {
            return Date(random.nextLong(first, middle))
        }

        private fun randomRegisterTime(random: Random): Date {
            return Date(random.nextLong(middle, last))
        }
    }


}